# GitHub Browser

An open-source Android app for viewing Trending github repository

![](https://image.flaticon.com/icons/png/512/2282/2282482.png "GitHub Browser")

Introduction:
=====

>  GitHub Browser implement an Architecture which is mantainable many way to enhance ;)

GitHub Browser is my play project, where test my learnings and other architectures with my current stable tech-stack <br>It's pretty close implementation of full-fledged application, complete with Network Requests, Local DB, Material Design.

It developed with well tested code cherries like **Kotlin**, **RxJava**, **Jetpack** & **AndroidX**.
<br>
It's a well packaged code base

GitHub Browser is built to some well maintanaible project code base . I'm not a UX guy ,bt still try to design application for ease of use inspired from day-today web-sites/apps. Fade Transitions are just pretty much easy to acchive :D

hav some play to try around wit differnt library to get reach in more developer ease :B

Screenshots & Gifs:
=====
![](https://i.imgur.com/jUBT43y.png "Home Screen")

Build Instructions:
=====

1. Just Clone the Project

2. Gradle Sync and Rebuild

Design Decisions & Dependencies:
=====

### [Kotlin](https://kotlinlang.org/):

I am in love with Kotlin, looking at this transition in android development from java world to kotlin writing this beautiful code which is 

### [JetPack](https://developer.android.com/jetpack) - Architecture Components & AndroidX:
Would be a loss to build an application without these libraries. With Google advocating MVVM, and these libraries working so flawlessly with each other, it really leaves you no choice.
<br>**Room** - Database Layer
<br>**ViewModel** - Data safeguarding across system configuration changes
<br>**Lifecycle** - surfing the issues with Activities / Fragments namely when pushing data
<br>**AndroidX, Material Components** - For embracing Material Design and backporting API features to minSdk

### [JUnit4](https://junit.org/junit4/) + [Mockito](https://site.mockito.org/) - Unit Testing:
A gold combination for unit testing and mocking dependencies. Since the app follows SOLID, dependent layers can be easily mocked and tested in isolation.
<br>I'm using [Mockito-Kotlin](https://github.com/nhaarman/mockito-kotlin) for a simpler DSL (and not writing ``when`` because ‘when’ is a Kotlin keyword)

### some Gems

- [Gradle Kotlin DSL](https://docs.gradle.org/current/userguide/kotlin_dsl.html)
- [Dependency Updates Plugin](https://github.com/ben-manes/gradle-versions-plugin)

# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
# OkHttp
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }

# Picasso
-dontwarn com.squareup.okhttp.**
-dontwarn javax.annotation.** # JSR 305 annotations are for embedding nullability information.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase # A resource is loaded with a relative path so the package of this class must be preserved.
-dontwarn org.codehaus.mojo.animal_sniffer.* # Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn okhttp3.internal.platform.ConscryptPlatform # OkHttp platform used only on JVM and when Conscrypt dependency is available.

# Gson
-dontwarn com.google.gson.Gson$6

package com.adam.githubbrowser.homeScreen.model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.adam.githubbrowser.data.webservices.api.RepositoryDTO
import com.adam.githubbrowser.homescreen.model.RepoListC
import com.adam.githubbrowser.homescreen.model.RepoListRepo
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.exceptions.CompositeException
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ReposListTest {

    @get:Rule
    val testRule = InstantTaskExecutorRule()

    companion object {

        val successWithEmptyResult: Single<List<RepositoryDTO>> = Single.just(emptyList())

        val failureResponse: Single<List<RepositoryDTO>> =
            Single.error(CompositeException(Throwable()))

        val successResponse: Single<List<RepositoryDTO>> = Single.just(
            listOf(
                RepositoryDTO(
                    author = "author",
                    name = "name",
                    description = "description",
                    avatar = "avatar",
                    currentPeriodStars = 0,
                    forks = 0,
                    stars = 0,
                    language = "language",
                    languageColor = "languageColor",
                    url = "url"
                )
            )
        )
    }

    @Mock
    private lateinit var local: RepoListC.Local

    @Mock
    private lateinit var remote: RepoListC.Remote

    private lateinit var repo: RepoListC.Repository

    @Before
    fun beforeTest() {
        MockitoAnnotations.initMocks(this)
        repo = RepoListRepo(
            local = local,
            remote = remote,
            compositeDisposable = CompositeDisposable()
        )
    }
}
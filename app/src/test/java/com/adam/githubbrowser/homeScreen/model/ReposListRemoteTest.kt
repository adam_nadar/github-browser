package com.adam.githubbrowser.homeScreen.model

import com.adam.githubbrowser.data.webservices.RepositoryWebService
import com.adam.githubbrowser.data.webservices.api.RepositoryDTO
import com.adam.githubbrowser.homescreen.model.RepoListC
import com.adam.githubbrowser.homescreen.model.ReposListRemote
import io.reactivex.Single
import io.reactivex.exceptions.CompositeException
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ReposListRemoteTest {

    companion object {

        val successWithEmptyResult: Single<List<RepositoryDTO>> = Single.just(emptyList())

        val failureResponse: Single<List<RepositoryDTO>> =
            Single.error(CompositeException(Throwable()))

        val successResponse: Single<List<RepositoryDTO>> = Single.just(
            listOf(
                RepositoryDTO(
                    author = "author",
                    name = "name",
                    description = "description",
                    avatar = "avatar",
                    currentPeriodStars = 0,
                    forks = 0,
                    stars = 0,
                    language = "language",
                    languageColor = "languageColor",
                    url = "url"
                )
            )
        )
    }

    @Mock
    private lateinit var webService: RepositoryWebService

    private lateinit var remote: RepoListC.Remote

    @Before
    fun beforeTest() {
        MockitoAnnotations.initMocks(this)
        remote = ReposListRemote(webService = webService)
    }

    @Test
    fun verifyThatRemoteDataSourceEmitsSuccess() {
        `when`(webService.getStoreTypes()).thenReturn(successResponse)
        remote.getTrendingRepo()
            .test()
            .assertValue { it.isNotEmpty() && it.first().author == "author" }
    }

    @Test
    fun verifyThatRemoteDataSourceEmitsFailure() {
        `when`(webService.getStoreTypes()).thenReturn(failureResponse)
        remote.getTrendingRepo()
            .test()
            .assertError { it is CompositeException }
    }

    @Test
    fun verifyThatRemoteDataSourceEmitsEmptyResult() {
        `when`(webService.getStoreTypes()).thenReturn(successWithEmptyResult)
        remote.getTrendingRepo()
            .test()
            .assertValue { it.isEmpty() }
    }
}
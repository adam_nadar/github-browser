package com.adam.githubbrowser.data.appdb

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.adam.githubbrowser.data.appdb.dao.RepositoryDao
import com.adam.githubbrowser.data.appdb.entites.RepositoryDBO

@Database(
    entities = [RepositoryDBO::class],
    version = INITIAL_RELEASE,
    exportSchema = true
)
abstract class AppDb : RoomDatabase() {
    companion object {
        const val TAG = "AppDb"
        private const val DATABASE_NAME = "AppDatabase"

        private var sInstance: AppDb? = null

        fun getDatabase(context: Context): AppDb {
            if (sInstance == null) {
                synchronized(this) {
                    if (sInstance == null)
                        sInstance = Room.databaseBuilder(
                            context,
                            AppDb::class.java,
                            DATABASE_NAME
                        )
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }
            return sInstance as AppDb
        }
    }

    abstract fun repositoryDao(): RepositoryDao

}
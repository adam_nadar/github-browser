package com.adam.githubbrowser.data.appdb.entites

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.adam.githubbrowser.data.webservices.api.RepositoryDTO

@Entity(tableName = "Repository")
data class RepositoryDBO(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val author: String,
    val name: String,
    val avatar: String,
    val url: String,
    val description: String,
    val language: String,
    val languageColor: String,
    val stars: Long,
    val forks: Long,
    val currentPeriodStars: Int
) {
    constructor(remoteModel: RepositoryDTO) : this(
        author = remoteModel.author,
        name = remoteModel.name,
        description = remoteModel.description,
        avatar = remoteModel.avatar,
        currentPeriodStars = remoteModel.currentPeriodStars,
        forks = remoteModel.forks,
        stars = remoteModel.stars,
        url = remoteModel.url,
        language = remoteModel.language.orEmpty(),
        languageColor = remoteModel.languageColor.orEmpty()
    )
}
package com.adam.githubbrowser.data.appdb.dao

import androidx.room.*
import com.adam.githubbrowser.data.appdb.entites.RepositoryDBO
import io.reactivex.Flowable

@Dao
abstract class RepositoryDao {

    @Query(value = "SELECT * FROM Repository ORDER BY stars ASC")
    abstract fun getAllRepositoryOrderByStarUpdates(): Flowable<List<RepositoryDBO>>

    @Query(value = "SELECT * FROM Repository ORDER BY name ASC")
    abstract fun getAllRepositoryOrderByNameUpdates(): Flowable<List<RepositoryDBO>>

    @Query(value = "DELETE FROM Repository")
    abstract fun deleteRepository()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAllRepository(dataSet: List<RepositoryDBO>)

    @Transaction
    open fun deleteAndInsertAll(dataSet: List<RepositoryDBO>) {
        deleteRepository()
        insertAllRepository(dataSet = dataSet)
    }
}
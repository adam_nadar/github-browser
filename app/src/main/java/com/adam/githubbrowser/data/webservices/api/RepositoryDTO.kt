package com.adam.githubbrowser.data.webservices.api

import com.google.gson.annotations.SerializedName

data class RepositoryDTO(
    @SerializedName("author")
    val author: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("avatar")
    val avatar: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("language")
    val language: String?,
    @SerializedName("languageColor")
    val languageColor: String?,
    @SerializedName("stars")
    val stars: Long,
    @SerializedName("forks")
    val forks: Long,
    @SerializedName("currentPeriodStars")
    val currentPeriodStars: Int
)
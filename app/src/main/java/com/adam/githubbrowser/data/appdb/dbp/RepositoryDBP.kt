package com.adam.githubbrowser.data.appdb.dbp

import android.content.Context
import com.adam.githubbrowser.data.appdb.AppDb
import com.adam.githubbrowser.data.appdb.entites.RepositoryDBO
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class RepositoryDBP @Inject constructor(@ApplicationContext context: Context) {

    private val db = AppDb.getDatabase(context).repositoryDao()

    fun getAllRepositoryOrderByStarUpdates() =
        db.getAllRepositoryOrderByStarUpdates()

    fun getAllRepositoryOrderByNameUpdates() =
        db.getAllRepositoryOrderByNameUpdates()

    fun deleteAndInsertAllTransaction(dataSet: List<RepositoryDBO>) =
        db.deleteAndInsertAll(dataSet = dataSet)

}
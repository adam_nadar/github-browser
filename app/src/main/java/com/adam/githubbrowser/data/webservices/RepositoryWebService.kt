package com.adam.githubbrowser.data.webservices

import com.adam.githubbrowser.data.webservices.api.RepositoryDTO
import io.reactivex.Single
import retrofit2.http.GET

interface RepositoryWebService {

    @GET("/repositories")
    fun getStoreTypes(): Single<List<RepositoryDTO>>

}





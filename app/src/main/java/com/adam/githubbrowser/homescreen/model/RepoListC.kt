package com.adam.githubbrowser.homescreen.model

import androidx.lifecycle.MutableLiveData
import com.adam.githubbrowser.data.appdb.entites.RepositoryDBO
import com.adam.githubbrowser.data.webservices.api.RepositoryDTO
import com.adam.githubbrowser.homescreen.uimodels.RepoListUiModel
import com.adam.githubbrowser.utils.processingstates.State
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface RepoListC {

    interface Repository {
        companion object {
            const val SORT_BY_STARS: Int = 1
            const val SORT_BY_NAME: Int = 2
        }

        val repoList: MutableLiveData<State<List<RepoListUiModel>>>
        fun getTrendingRepo(force: Boolean, sortBy: Int)
    }

    interface Local {
        fun getTrendingRepo(sortBy: Int): Flowable<List<RepositoryDBO>>
        fun cacheRepositories(dataSet: List<RepositoryDBO>): Completable
    }

    interface Remote {
        fun getTrendingRepo(): Single<List<RepositoryDTO>>
    }
}
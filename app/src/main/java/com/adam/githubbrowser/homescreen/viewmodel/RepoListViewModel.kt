package com.adam.githubbrowser.homescreen.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.adam.githubbrowser.homescreen.model.RepoListC
import com.adam.githubbrowser.homescreen.uimodels.RepoListUiModel
import com.adam.githubbrowser.utils.processingstates.State
import io.reactivex.disposables.CompositeDisposable

class RepoListViewModel  @ViewModelInject constructor(
    private val repo: RepoListC.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel() {

    companion object {
        const val SORT_BY_STARS: Int = RepoListC.Repository.SORT_BY_STARS
        const val SORT_BY_NAME: Int = RepoListC.Repository.SORT_BY_NAME
    }

    val repoListLiveData: LiveData<State<List<RepoListUiModel>>> = repo.repoList

    fun getTrendingRepo(force: Boolean = false, sortBy: Int = SORT_BY_STARS) =
        repo.getTrendingRepo(sortBy = sortBy, force = force)

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}
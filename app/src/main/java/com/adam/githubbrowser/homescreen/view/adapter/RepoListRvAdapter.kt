package com.adam.githubbrowser.homescreen.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.toColorInt
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.adam.githubbrowser.R
import com.adam.githubbrowser.homescreen.uimodels.RepoListUiModel
import com.adam.githubbrowser.utils.extensions.setRemoteImage
import io.reactivex.subjects.Subject
import kotlinx.android.synthetic.main.item_repository.view.*
import java.text.NumberFormat
import java.util.*

class RepoListRvAdapter(
    private val dataSet: MutableList<RepoListUiModel> = mutableListOf(),
    private val subject: Subject<RepoListUiModel>
) : RecyclerView.Adapter<RepoListRvAdapter.RepoListRvViewHolder>() {

    fun setData(dataSet: List<RepoListUiModel>) {
        with(this.dataSet) {
            clear()
            addAll(dataSet)
        }
        notifyDataSetChanged()
    }

    fun setItemExpanded(uiModel: RepoListUiModel) {

        val lastSelectedItemPosition = dataSet.indexOfFirst { it.isSelected }
        val selectedItemPosition = dataSet.indexOf(uiModel)

        dataSet.forEachIndexed { index, repoListUiModel ->
            if (uiModel.id == repoListUiModel.id && uiModel.isSelected) {
                repoListUiModel.isSelected = false
                notifyItemChanged(index)
                return
            }
        }

        if (lastSelectedItemPosition != -1) {
            dataSet[lastSelectedItemPosition].isSelected = false
            notifyItemChanged(lastSelectedItemPosition)
        }

        if (selectedItemPosition != -1) {
            dataSet[selectedItemPosition].isSelected = true
            notifyItemChanged(selectedItemPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoListRvViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_repository, parent, false)
        return RepoListRvViewHolder(itemView = itemView)
    }

    override fun getItemCount(): Int = dataSet.count()

    override fun onBindViewHolder(holder: RepoListRvViewHolder, position: Int) {
        holder.bind(item = dataSet[position])
    }

    inner class RepoListRvViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: RepoListUiModel) {
            with(itemView) {

                civAvatar.setRemoteImage(imageUrl = item.avatar, onErrorBlock = {
                    civAvatar.setImageResource(R.drawable.placeholder_gradient)
                })

                groupCollapsedState.isVisible = item.isSelected

                tvRepoAuthor.text = item.author

                tvRepoName.text = item.name

                tvRepoDescription.text = item.description

                if (item.languageColor.isNotBlank())
                    ivLanguageColor.setColorFilter(
                        item.languageColor.toColorInt(),
                        android.graphics.PorterDuff.Mode.SRC_IN
                    )

                tvLanguage.text = item.language

                tvStar.text = NumberFormat.getNumberInstance(Locale.getDefault()).format(item.stars)

                tvForks.text =
                    NumberFormat.getNumberInstance(Locale.getDefault()).format(item.forks)

                setOnClickListener { subject.onNext(item) }
            }
        }

    }
}
package com.adam.githubbrowser.homescreen.model

import androidx.lifecycle.MutableLiveData
import com.adam.githubbrowser.data.appdb.entites.RepositoryDBO
import com.adam.githubbrowser.homescreen.model.RepoListC.Repository.Companion.SORT_BY_STARS
import com.adam.githubbrowser.homescreen.uimodels.RepoListUiModel
import com.adam.githubbrowser.utils.extensions.addTo
import com.adam.githubbrowser.utils.processingstates.State
import com.adam.githubbrowser.utils.processingstates.State.Companion.publishData
import com.adam.githubbrowser.utils.processingstates.State.Companion.publishError
import com.adam.githubbrowser.utils.processingstates.State.Companion.publishLoading
import com.adam.githubbrowser.utils.synk.Synk
import com.adam.githubbrowser.utils.synk.SynkKeys.REPOSITORY_LIST
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class RepoListRepo(
    private val local: RepoListC.Local,
    private val remote: RepoListC.Remote,
    private val compositeDisposable: CompositeDisposable
) : RepoListC.Repository {

    companion object {
        const val TAG = "RepoListRepo"
    }

    private var sortBy: Int = SORT_BY_STARS

    private var disposable: Disposable? = null

    override val repoList: MutableLiveData<State<List<RepoListUiModel>>> = MutableLiveData()

    override fun getTrendingRepo(force: Boolean, sortBy: Int) {
        if (force.not() && repoList.value == null || sortBy != this.sortBy) {
            disposable?.dispose()
            repoList.value = publishLoading()
            disposable = local.getTrendingRepo(sortBy = sortBy)
                .map {
                    this.sortBy = sortBy
                    return@map publishData(it.map(::RepoListUiModel))
                }
                .onErrorReturn { ex -> publishError(ex) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    repoList.value = it
                }

            disposable?.addTo(compositeDisposable = compositeDisposable)
        }

        if (force || Synk.shouldSync(key = REPOSITORY_LIST, window = 2, unit = TimeUnit.HOURS)) {
            repoList.value = publishLoading()
            remote.getTrendingRepo()
                .map {
                    return@map it.map(::RepositoryDBO)
                }
                .flatMapCompletable(local::cacheRepositories)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { Synk.syncSuccess(key = REPOSITORY_LIST) },
                    {
                        repoList.value = publishError(Throwable())
                        Synk.syncFailure(key = REPOSITORY_LIST)
                    }
                )
                .addTo(compositeDisposable = compositeDisposable)
        }
    }
}

package com.adam.githubbrowser.homescreen.model

import com.adam.githubbrowser.data.webservices.RepositoryWebService
import com.adam.githubbrowser.data.webservices.api.RepositoryDTO
import io.reactivex.Single

class ReposListRemote(
    private val webService: RepositoryWebService
) : RepoListC.Remote {

    override fun getTrendingRepo(): Single<List<RepositoryDTO>> {
        return webService.getStoreTypes()
    }
}
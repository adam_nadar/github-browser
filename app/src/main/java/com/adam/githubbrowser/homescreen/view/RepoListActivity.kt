package com.adam.githubbrowser.homescreen.view

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import com.adam.githubbrowser.R
import com.adam.githubbrowser.baseui.BaseActivity
import com.adam.githubbrowser.homescreen.uimodels.RepoListUiModel
import com.adam.githubbrowser.homescreen.view.adapter.RepoListRvAdapter
import com.adam.githubbrowser.homescreen.viewmodel.RepoListViewModel
import com.adam.githubbrowser.homescreen.viewmodel.RepoListViewModel.Companion.SORT_BY_NAME
import com.adam.githubbrowser.homescreen.viewmodel.RepoListViewModel.Companion.SORT_BY_STARS
import com.adam.githubbrowser.utils.processingstates.State
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.main_repository_error.*

@AndroidEntryPoint
class RepoListActivity : BaseActivity() {

    override val layoutRes: Int = R.layout.activity_main

    override val menuRes: Int = R.menu.menu_main_activity

    private val subject: Subject<RepoListUiModel> by lazy { PublishSubject.create<RepoListUiModel>() }

    private val adp: RepoListRvAdapter by lazy { RepoListRvAdapter(subject = subject) }

    private var disposable: Disposable? = null

    private val viewModel : RepoListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpToolbar()
        setUpUi()
        setUpLiveDataListeners()
        fetchData()
    }

    private fun setUpLiveDataListeners() {
        viewModel.repoListLiveData.observe(this, Observer {
            when (it) {
                is State.Loading -> {
                    rvRepoList.isGone = true
                    sfProgress.isVisible = true
                    inc_error.isGone = true
                    sfProgress.startShimmer()
                }
                is State.Data -> {
                    if (it.data.isNotEmpty()) {
                        sflRepoList.isRefreshing = false
                        sfProgress.stopShimmer()
                        sfProgress.isGone = true
                        rvRepoList.isVisible = true
                        adp.setData(dataSet = it.data)
                    }
                }
                is State.Error -> {
                    sflRepoList.isRefreshing = false
                    sfProgress.isGone = true
                    rvRepoList.isGone = true
                    inc_error.isVisible = true
                    sfProgress.stopShimmer()
                    Log.e(TAG, "error", it.e)
                }
            }
        })
    }

    private fun fetchData() {
        viewModel.getTrendingRepo()
    }

    private fun setUpUi() {
        rvRepoList.adapter = adp

        val dividerItemDecor = DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL)

        ContextCompat.getDrawable(context, R.drawable.divider_rv)
            ?.let(dividerItemDecor::setDrawable)

        rvRepoList.addItemDecoration(dividerItemDecor)

        disposable = subject.subscribe(::markItemExpanded)

        btnRetry.setOnClickListener { viewModel.getTrendingRepo(force = true) }
        sflRepoList.setOnRefreshListener { viewModel.getTrendingRepo(force = true) }
    }

    private fun markItemExpanded(uiModel: RepoListUiModel) {
        Log.e(TAG, uiModel.toString())
        adp.setItemExpanded(uiModel = uiModel)
    }

    private fun setUpToolbar() {
        setSupportActionBar(inc_toolbar as Toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuSortByStars -> viewModel.getTrendingRepo(sortBy = SORT_BY_STARS)
            R.id.menuSortByName -> viewModel.getTrendingRepo(sortBy = SORT_BY_NAME)
        }
        return true
    }

    override fun onDestroy() {
        disposable?.dispose()
        super.onDestroy()
    }

    companion object {
        private const val TAG = "RepoListActivity"
    }
}

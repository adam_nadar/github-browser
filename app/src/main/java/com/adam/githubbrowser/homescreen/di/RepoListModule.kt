package com.adam.githubbrowser.homescreen.di

import com.adam.githubbrowser.data.appdb.dbp.RepositoryDBP
import com.adam.githubbrowser.data.webservices.RepositoryWebService
import com.adam.githubbrowser.homescreen.model.RepoListC
import com.adam.githubbrowser.homescreen.model.RepoListRepo
import com.adam.githubbrowser.homescreen.model.ReposListLocal
import com.adam.githubbrowser.homescreen.model.ReposListRemote
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

@Module
@InstallIn(ActivityComponent::class)
class RepoListModule {

    @Provides
    fun providesRepo(
        remote: RepoListC.Remote,
        local: RepoListC.Local,
        compositeDisposable: CompositeDisposable
    ): RepoListC.Repository {
        return RepoListRepo(
            local = local,
            remote = remote,
            compositeDisposable = compositeDisposable
        )
    }

    @Provides
    fun providesWs(retrofit: Retrofit): RepositoryWebService =
        retrofit.create(RepositoryWebService::class.java)

    @Provides
    fun providesRDS(
        repositoryWebService: RepositoryWebService
    ): RepoListC.Remote {
        return ReposListRemote(webService = repositoryWebService)
    }

    @Provides
    fun providesLDS(
        repositoryDBP: RepositoryDBP
    ): RepoListC.Local {
        return ReposListLocal(repositoryDBP = repositoryDBP)
    }

    @Provides
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()
}
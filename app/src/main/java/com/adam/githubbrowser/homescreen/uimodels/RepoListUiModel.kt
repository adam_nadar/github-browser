package com.adam.githubbrowser.homescreen.uimodels

import com.adam.githubbrowser.data.appdb.entites.RepositoryDBO

data class RepoListUiModel(
    val id: Long,
    val avatar: String,
    val author: String,
    val name: String,
    val description: String,
    val languageColor: String,
    val language: String,
    val stars: Long,
    val forks: Long,
    val currentPeriodStars: Int,
    var isSelected: Boolean = false
) {
    constructor(databaseModel: RepositoryDBO) : this(
        id = databaseModel.id,
        languageColor = databaseModel.languageColor,
        language = databaseModel.language,
        stars = databaseModel.stars,
        forks = databaseModel.forks,
        currentPeriodStars = databaseModel.currentPeriodStars,
        avatar = databaseModel.avatar,
        description = databaseModel.description,
        name = databaseModel.name,
        author = databaseModel.author
    )
}
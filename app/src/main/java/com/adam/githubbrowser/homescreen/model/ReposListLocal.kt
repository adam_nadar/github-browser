package com.adam.githubbrowser.homescreen.model

import com.adam.githubbrowser.data.appdb.dbp.RepositoryDBP
import com.adam.githubbrowser.data.appdb.entites.RepositoryDBO
import com.adam.githubbrowser.homescreen.model.RepoListC.Repository.Companion.SORT_BY_NAME
import com.adam.githubbrowser.homescreen.model.RepoListC.Repository.Companion.SORT_BY_STARS
import com.adam.githubbrowser.utils.extensions.toCompletable
import io.reactivex.Completable
import io.reactivex.Flowable

class ReposListLocal(
    private val repositoryDBP: RepositoryDBP
) : RepoListC.Local {

    override fun cacheRepositories(dataSet: List<RepositoryDBO>): Completable {
        return {
            repositoryDBP.deleteAndInsertAllTransaction(dataSet = dataSet)
        }.toCompletable()
    }

    override fun getTrendingRepo(sortBy: Int): Flowable<List<RepositoryDBO>> {
        return when (sortBy) {
            SORT_BY_STARS -> repositoryDBP.getAllRepositoryOrderByStarUpdates()
            SORT_BY_NAME -> repositoryDBP.getAllRepositoryOrderByNameUpdates()
            else -> throw IllegalArgumentException("Illegal sortBy argument, argument can be SORT_BY_STARS or SORT_BY_STARS")
        }
    }
}
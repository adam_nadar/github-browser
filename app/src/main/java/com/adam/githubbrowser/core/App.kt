package com.adam.githubbrowser.core

import android.app.Application
import com.adam.githubbrowser.BuildConfig
import com.adam.githubbrowser.utils.synk.Synk
import com.facebook.stetho.Stetho
import com.squareup.picasso.Picasso
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class App : Application() {

    @Inject
    lateinit var picasso: Picasso

    override fun onCreate() {
        super.onCreate()

        initialisePicasso()
        initialiseSynk()
        initialiseStetho()
    }

    private fun initialiseStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(applicationContext)
        }
    }

    private fun initialiseSynk() {
        Synk.init(context = this)
    }

    private fun initialisePicasso() {
        // To make sure Picasso (with Okhttp3Downloader) is initialized only once
        Picasso.setSingletonInstance(picasso)
    }
}
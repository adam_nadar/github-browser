package com.adam.githubbrowser.utils

object Constants {
    const val NETWORK_REQUEST_TIMEOUT: Long = 30

    const val CONTENT_TYPE_KEY = "content-type"
    const val CONTENT_TYPE_VALUE = "application/json; charset=utf-8"
}
package com.adam.githubbrowser.utils.extensions

import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/*Disposable*/
fun Disposable.addTo(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(this)
}

//Completable
fun <R> (() -> R).toCompletable(): Completable {
    return Completable.create { this() }
}
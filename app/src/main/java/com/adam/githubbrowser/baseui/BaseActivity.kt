package com.adam.githubbrowser.baseui

import android.content.Context
import android.os.Bundle
import android.view.Menu
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    companion object {
        const val NO_VALUE = -1
    }

    protected val context: Context by lazy { this }

    /**
     * Override to provide a Layout Resource for inflating the View.
     */
    @LayoutRes
    protected open val layoutRes: Int = NO_VALUE

    /**
     * Override to provide a Layout Resource for inflating the View.
     */
    @MenuRes
    protected open val menuRes: Int = NO_VALUE

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (layoutRes != NO_VALUE) setContentView(layoutRes)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (menuRes != NO_VALUE) {
            menuInflater.inflate(menuRes, menu)
            return true
        }
        return super.onCreateOptionsMenu(menu)
    }
}
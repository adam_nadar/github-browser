object Config {
    const val appId = "com.adam.githubbrowser"

    const val debug_minifyEnabled = false

    const val release_minifyEnabled = true

}

const val kotlinVersion = "1.3.50"
const val hiltVersion = "2.30.1-alpha"

object BuildPlugins {
    private object Versions {
        const val androidBuildToolsVersion = "3.5.3"
    }

    const val androidGradlePlugin =
        "com.android.tools.build:gradle:${Versions.androidBuildToolsVersion}"
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
    const val hiltClassPath = "com.google.dagger:hilt-android-gradle-plugin:$hiltVersion"

    const val androidApplication = "com.android.application"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinAndroidExtensions = "kotlin-android-extensions"
    const val kotlinKapt = "kotlin-kapt"
    const val hiltPlugin = "dagger.hilt.android.plugin"
}

object AndroidSdk {
    const val min = 19
    const val compile = 29
    const val target = compile
}

object Urls {
    const val staging = "https://private-anon-86593d072b-githubtrendingapi.apiary-proxy.com"
    const val production = "https://github-trending-api.now.sh"
}

object Libraries {
    private object Versions {
        const val jetpack = "1.1.0"
        const val constraintLayout = "1.1.3"
        const val swiperefreshlayout = "1.0.0"
        const val recyclerview = "1.1.0"
        const val circleimageview = "3.0.1"
        const val materialComponent = "1.0.0"
        const val preference = "1.1.0"
        const val ktx = "1.3.2"
        const val ktxFragment = "1.2.5"
        const val hiltViewmodel = "1.0.0-alpha01"
        const val okhttp = "4.2.2"
        const val retrofit = "2.6.3"
        const val picasso = "2.5.2"
        const val rxjava = "2.2.15"
        const val rxAndroid = "2.1.1"
        const val gson = "2.8.6"
        const val gsonConverter = "2.6.3"
        const val retrofitRxAdapter = "2.6.3"
        const val picassoDownloader = "1.1.0"
        const val ktxLifecycleViewmodel = "2.1.0"
        const val ktxViewModelRuntime = "2.2.0"
        const val ktxViewModelLifeCycleCompiler = "2.1.0"
        const val stetho = "1.5.0"

        const val facebookShimmerSdk = "0.5.0"

        const val aacRoom = "2.2.2"
        const val aacRoomRx = aacRoom

    }

    val stetho = "com.facebook.stetho:stetho:${Versions.stetho}"
    val stethoNetworkInterceptor = "com.facebook.stetho:stetho-okhttp3:${Versions.stetho}"
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.jetpack}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val swiperefreshlayout =
        "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swiperefreshlayout}"
    const val recyclerview = "androidx.recyclerview:recyclerview:${Versions.recyclerview}"
    const val circleimageview = "de.hdodenhof:circleimageview:${Versions.circleimageview}"
    const val materialComponent =
        "com.google.android.material:material:${Versions.materialComponent}"
    const val preference = "androidx.preference:preference-ktx:${Versions.preference}"
    const val ktxCore = "androidx.core:core-ktx:${Versions.ktx}"
    const val ktxFragment = "androidx.fragment:fragment-ktx:${Versions.ktxFragment}"
    const val hiltAndroid = "com.google.dagger:hilt-android:${hiltVersion}"
    const val hiltAndroidCompiler = "com.google.dagger:hilt-android-compiler:${hiltVersion}"
    const val hiltViewModel = "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hiltViewmodel}"
    const val hiltCompiler = "androidx.hilt:hilt-compiler:${Versions.hiltViewmodel}"
    const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val picasso = "com.squareup.picasso:picasso:${Versions.picasso}"
    const val picassoDownloader =
        "com.jakewharton.picasso:picasso2-okhttp3-downloader:${Versions.picassoDownloader}"
    const val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
    const val rxjava = "io.reactivex.rxjava2:rxjava:${Versions.rxjava}"
    const val retrofitRxAdapter =
        "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofitRxAdapter}"

    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    const val gsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.gsonConverter}"

    const val ktxViewModelLifeCycleCompiler =
        "androidx.lifecycle:lifecycle-compiler:${Versions.ktxViewModelLifeCycleCompiler}"
    const val ktxLifecycleViewmodel =
        "androidx.lifecycle:lifecycle-extensions:${Versions.ktxLifecycleViewmodel}"
    const val ktxViewModelRuntime =
        "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.ktxViewModelRuntime}"
    const val facebookShimmer = "com.facebook.shimmer:shimmer:${Versions.facebookShimmerSdk}"

    const val aacRoomRuntime = "androidx.room:room-runtime:${Versions.aacRoom}"
    const val kapt_aacRoomCompiler = "androidx.room:room-compiler:${Versions.aacRoom}"
    const val aacRoomRx = "androidx.room:room-rxjava2:${Versions.aacRoomRx}"
}

object TestLibraries {
    private object Versions {
        const val junit4 = "4.12"
        const val extJunit = "1.1.1"
        const val test_okhttp = "4.2.2"
        const val test_liveData = "2.1.0"
        const val espresso = "3.1.0"
        const val testAacRoom = "2.2.2"
        const val mockito = "1.+"
        const val liveData = "1.1.1"
    }

    const val junit4 = "junit:junit:${Versions.junit4}"
    const val extJunit = "androidx.test.ext:junit:${Versions.extJunit}"
    const val test_okhttp = "com.squareup.okhttp3:mockwebserver:${Versions.test_okhttp}"
    const val test_liveData = "androidx.arch.core:core-testing:${Versions.test_liveData}"
    const val espresso =
        "androidx.test.espresso:espresso-core:${Versions.espresso}"
    const val test_Room = "androidx.room:room-testing:${Versions.testAacRoom}"
    const val mockito = "org.mockito:mockito-core:${Versions.mockito}"
    const val liveData = "com.jraska.livedata:testing-ktx:${Versions.liveData}"

}